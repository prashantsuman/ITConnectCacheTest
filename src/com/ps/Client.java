package com.ps;

import java.util.Objects;

import com.ps.service.CacheSample;
import com.ps.service.CacheSampleExecutor;

public class Client {

	public static void main(String[] args) {
		String s = new String("Test");
		/*
		 * Create an instance of CachedSample, set the minutesToLive to 1 minute. Give
		 * the object some unique identifier.
		 */
		CacheSample co = new CacheSample(s, new Long(1234), 10);
		/* Place the object into the cache! */
		CacheSampleExecutor.putCache(co);
		/* Try to retrieve the object from the cache! */
		CacheSample cachedData = (CacheSample) CacheSampleExecutor.getCache(new Long(1234));
		if(Objects.isNull(cachedData)) {
			System.out.println("Fail: Object not exist in cache.");
		}
		else {
			System.out.println("Success: Cached data ="+(String)cachedData.getValue().toString());
		}

	}
}
