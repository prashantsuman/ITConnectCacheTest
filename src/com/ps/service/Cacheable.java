package com.ps.service;

public interface Cacheable {
	public boolean isExpired();

	public Object getIdentifier();
}
